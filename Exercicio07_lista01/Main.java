import java.util.Scanner;

public class Main
{
    public static void Main(String[] args) {
        
        Scanner le = new Scanner(System.in);   
        Pescador p = new Pescador();
            
        System.out.println("Informe o nome do pescador: ");
        p.setNome(le.next());
            
        System.out.println("Informe a UF do pescador: ");
        p.setUf(le.next());
        
        System.out.println("Informe o cep do pescador: ");
        p.setCep(le.next());
            
        System.out.println("Informe o número da sua lincença de pescador: ");
        p.setNumLicencaPesc(le.next());
            
        while(true) {
            Peixe pei = new Peixe();
            System.out.println("Informe o peso do peixe pescado: ");
            pei.setPeso(le.nextDouble());
            
            p.addPeixes(pei);
            
            System.out.println("Gostaria de adicionar mais peixes? (S)im/(N)ão");
            if(le.next().equalsIgnoreCase("N")) {
                break;   
            }
        }
    
         System.out.println("O pescador " + p.getNome() + " deverá pagar " + p.multaSobrePesoPeixes() + " reais de multa.");
        
        }
    }

