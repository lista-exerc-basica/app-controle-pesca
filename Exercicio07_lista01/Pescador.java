import java.util.ArrayList;


public class Pescador
{
    private String nome, uf, cep, numLicencaPesc;
    private ArrayList<Peixe> peixesPescados;
    
    public Pescador(){
        this.peixesPescados = new ArrayList<>();
    }
    
    public Pescador(String nome, String uf, String cep, String numLicencaPesc) {
        this.nome = nome;
        this.uf = uf;
        this.cep = cep;
        this.numLicencaPesc = numLicencaPesc;
    }
    
    public double multaSobrePesoPeixes() {
        double pesoExcedido = 0;
        
        for(Peixe p : peixesPescados) {
            if(p.getPeso() > 50.0) {
                pesoExcedido += (p.getPeso() - 50.0);   
            }
        }
        
        return pesoExcedido * 4.0;
    }
      
    public void addPeixes(Peixe peixesPescados) {
        this.peixesPescados.add(peixesPescados);   
    }
    
     public void removePeixes(Peixe peixesPescados) {
         this.peixesPescados.remove(peixesPescados);   
    }
    
    public ArrayList<Peixe> getPeixesPescados() {
        return  this.peixesPescados;   
    }
    
    public void setNome(String nome) {
        this.nome = nome;   
    }
    
    public String getNome() {
        return this.nome;   
    }
    
    public void setUf(String uf) {
        this.uf = uf;   
    }
    
    public String getUf() {
        return this.uf;   
    }
    
    public void setCep(String cep) {
        this.cep = cep;   
    }
    
    public String getCep() {
        return this.cep;   
    }  
    
    public void setNumLicencaPesc(String numLicencaPesc) {
        this.numLicencaPesc = numLicencaPesc;   
    }
    
    public String getNumLicencaPesc() {
        return this.numLicencaPesc;   
    }

}
